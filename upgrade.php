<?php
require 'functions/mainconn.php';
?>
<html>
<head>
<title>Hostclaw</title>

<link rel="stylesheet" href="admin/stylesheet.css" type="text/css">

  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
  
  <script src="admin/validation/jquery.validate.js" type="text/javascript"></script>
  
  <style type="text/css">
  label { width: 10em; float: left; }
  label.error { float: none; color: red; margin-left: 0.5em; margin-top: vertical-align: top; padding: 8px; background-color: #dcdcdc; }
  p { clear: both; }
  .submit { margin-left: 12em; }
  em { font-weight: bold; padding-right: 1em; vertical-align: top; }
  .required {  font-family: Verdana, Arial, Helvetica, sans-serif; 
   font-size: 24px; 
   margin-right: 6px; 
   background-color: #CCCCCC;
   border: 1px solid #666666;
  }
  </style>

  
<?php

if ($panelused = "cpanel"){
	require("functions/whm.php"); 
  $whm= new whm;
  $whm->init("$whmhost","$whmuser","$whmaccesshash");      
  $version= $whm->version(); }

if ($panelused = "directadmin"){
	require 'functions/directadmin.php';
	$sock = new HTTPSocket;
	$sock->connect("$da_domain",2222);
	$sock->set_login("$da_user","$da_pass");
	$sock->query('/CMD_API_PACKAGES_RESELLER');
	$pkgs = $sock->fetch_parsed_body();
	ksort($pkgs);
	//print_r($pkgs);
	$counter='0';
}

?>
  
  <script type="text/javascript">
  $(document).ready(function(){
      $("form").validate(
      {
          rules: {
              accountpassword: {
                  required: true,
                  maxlength: 14
              },
              accountusername: {
                  required: true,
                  maxlength: 8
              }
          }
      }
      );
  });
  </script>
  
</head>

<body>

<div id="navcontainer">
<ul id="navlist">
<li><a>Home</a></li>
<li><a href="index.php">My form</a></li>
<li><a href="upgrade.php">Upgrade</a></li>
<li><a href="logout.php">Logout</a></li>
</ul>
</div>
<div class="header">Hosting Package Upgrader</div>

<div class="offset">
<br>
<table cellpadding="5">

<?php
//////////////////////////////////////// confirm your package
if ($_POST['changemypackage']){ 
    $hostingpackage = $_POST[hostingpackage];
    $hostingpackageC = explode("_", $hostingpackage); $hostingpackageC = $hostingpackageC[1];
    $accountusername = $_POST[accountusername];
    
    $hideme1 = "yes"; $hideme2 = "yes";
    
    $datask = $whm->changepackage($accountusername,$hostingpackage);
    mysql_query("UPDATE orders SET pack='$hostingpackage' WHERE username='$accountusername' ") or die (mysql_error());
    
    if ($datask === false) {
        echo "
        <tr><td colspan=\"2\" bgcolor=\"#C0C0C0\">Result!</td></tr>
        <tr><td colspan=\"2\"><img src=\"admin/graphics/tick.png\" align=\"left\">
        The account username <b>$accountusername</b> now has been upgraded to the <b>$hostingpackageC</b> package.
        <br>
        </td></tr>
        ";
    } else {
        echo "<tr><td colspan=\"2\" bgcolor=\"#C0C0C0\">ERROR</td></tr>
        <tr><td colspan=\"2\"><strong>Error whilst upgrading your hosting account. Forward to technical support.</strong><br>"; print_r($task2->errors); echo "</td></tr>"; 
    }
        
}

//////////////////////////////////////// select a package
if ($_POST['checkname']){ 
    $accountusername = $_POST[accountusername];
    $accountpassword = $_POST[accountpassword];
    $fetchaccount = mysql_query("SELECT * FROM orders WHERE username='$accountusername' && password='$accountpassword' ");
    $countaccount = mysql_num_rows($fetchaccount); $myoacc = mysql_fetch_array($fetchaccount); $dapass = $myoacc['password'];
    $fetch=$whm->accountsummary($accountusername);
    
    $debug = false; 
    
    if ($fetch === false) {
		echo "
		<tr><td colspan=\"2\" bgcolor=\"#C0C0C0\">FAILURE!</td></tr>
		<tr><td colspan=\"2\"><img src=\"admin/graphics/warning.png\" align=\"left\">The cpanel username $accountname could not be found. "; print_r($task2->errors); echo "
		</td></tr>";
    //} elseif ($countaccount > 0 && $accountpassword == $dapass && $debug = false){
    } elseif ($countaccount < 0 && $accountpassword != $dapass || $dapass == null){
    echo "
		<tr><td colspan=\"2\" bgcolor=\"#C0C0C0\">FAILURE!</td></tr>
		<tr><td colspan=\"2\"><img src=\"admin/graphics/warning.png\" align=\"left\">The password you specified is incorrect. It may be different to your cpanel password if you've changed it. $countaccount $accountpassword $dapass
		</td></tr>";
    } else {
        $hideme1 = "yes";
        //var_dump($accountpassword);
        ?>
        <form method="POST" name="checkname">
        <tr><td colspan="2" bgcolor="#C0C0C0">Upgrade your account</td></tr>
        <tr><td colspan="2">
        <p>Choose your new hosting package.</p>
        </td></tr>
                
        <tr><td>Hosting Package: </td><td>
            <?php
            echo "<select   class=\"bigtext\"  style=\"width: 500px;\" name=\"hostingpackage\">";
            $daquery = mysql_query("SELECT * FROM packages");
            while($datax = mysql_fetch_array($daquery))  {	
                echo "<option value =\"$datax[1]\">$datax[2]</option>";
            }		
            echo "</select>	";
            ?>       
        </td></tr>
        <?php echo "<input type=\"hidden\" class=\"bigtext\" style=\"width: 500px;\" name=\"accountusername\" value=\"$accountusername\">"; ?>
        
        <tr><td colspan="2" align="right"><input type="submit" name="changemypackage"  class="bigbutton" value="Change my package"></td></tr>

        <?php
    } 
}
?>

<?php
if ($hideme1 != "yes"){ ?>
<form method="POST" name="checkname">
<tr><td colspan="2" bgcolor="#C0C0C0">Upgrade your account</td></tr>
<tr><td colspan="2">
<p>Here you can upgrade your account for in case you are running out of bandwidth of web space.</p>
<p>Once you choose what package you want to upgrade to, you won't have to pay anything until the next month.</p>
</td></tr>

<tr><td colspan="2" bgcolor="#C0C0C0">Enter your details</td></tr>
<tr><td colspan="2">These were the details you used on the checkout form.</td></tr>

<tr><td>Account Username: </td><td><input type="text" class="bigtext" style="width: 500px;" id="accountusername" name="accountusername" value=""></td></tr>
<tr><td>Account Password: </td><td><input type="password" class="bigtext" style="width: 500px;" id="accountpassword" name="accountpassword" value=""></td></tr>

<tr><td colspan="2" align="right"><input type="submit" name="checkname"  class="bigbutton" value="Confirm Details"></td></tr>
</form>
<?php } ?>


</table>


<br><br>

<!-- <a href="index.php">Back to Control Panel</a> -->
<br><br><div class="footer">Thanks for choosing <?php echo "$nameofmywebhost"; ?></div>

</div>


</body>
</html>