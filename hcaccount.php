<?php
// ob_Start;
include 'functions/config.php';
session_start(); 
header("Cache-control: private"); // IE 6 Fix. 

if (isset($_COOKIE['ahmreferrer'])){ $ahmreferrer = $_COOKIE['ahmreferrer']; }
else { $ahmreferrer = NULL; }

if (isset($_GET['page']) && $_GET['page'] == "logout"){
    setcookie("id", 'loggedout',time()+(60*60*24*5), '/');
    setcookie("pass", 'loggedout',time()+(60*60*24*5), '/');
    $notice = "You have been logged out.";
    header("Location: hcaccount.php?notice=$notice");
}

if (isset($_POST['newticket'])) {
    $datenumber = date("Y-m-d");
    $datetimenumber = date("Y-m-d H:s");
    $latestpond = mysql_fetch_array(mysql_query("SELECT * FROM tickets ORDER by pond DESC LIMIT 1"));
    $newpond = $latestpond['pond']+ 1;

    $addorder = mysql_query("INSERT INTO tickets 
    (userid, aboutuserid, pond, floor, 
    title, content, closed, datenumber, datetimenumber)
    VALUES('$logged[id]','$logged[id]','$newpond','1',
    '$_POST[title]','$_POST[message]','no','$datenumber', '$datetimenumber')") or die (mysql_error());

    $notice = "Your support ticket has been created.";
    header("Location: hcaccount.php?notice=$notice");
}  

if (isset($_GET['cpfm'])) {
  $myorder = mysql_query("SELECT * FROM orders WHERE id='$_GET[cpfm] && userid='$logged[id]'");
  $myorder = mysql_fetch_array($myorder);
  $newdate = strtotime ( '+28 days' , strtotime ( $myorder[expirydatenumber] ) ) ;
  $newdate = date ( 'Y-m-d' , $newdate );
  mysql_query("UPDATE orders SET paymentfrequency='monthly', expirydatenumber='$newdate' WHERE id='$_GET[cpfm]'");
  $notice = "The payment frequency has been updated to monthly.";
  header("Location: hcaccount.php?notice=$notice");
}

if (isset($_GET['cpfa'])) {
  $myorder = mysql_query("SELECT * FROM orders WHERE id='$_GET[cpfm] && userid='$logged[id]'");
  $myorder = mysql_fetch_array($myorder);
  $newdate = strtotime ( '+1 year' , strtotime ( $myorder[expirydatenumber] ) ) ;
  $newdate = date ( 'Y-m-d' , $newdate );
  mysql_query("UPDATE orders SET paymentfrequency='annualy', expirydatenumber='$newdate' WHERE id='$_GET[cpfm]'");
  $notice = "The payment frequency has been updated to annually   .";
  header("Location: hcaccount.php?notice=$notice");
}


if (isset($_POST['register'])) {
       // the above line checks to see if the html form has been submitted
       $username = $_POST['username'];
       $password = $_POST['pass'];
       $cpassword = $_POST['cpass'];
       $email = $_POST['email'];
       
       //the above lines set variables with the user submitted information
    if($username==NULL|$password==NULL|$cpassword==NULL|$email==NULL) {
       //checks to make sure no fields were left blank
       $notice = "A field was left blank.";
       header("Location: hcaccount.php?notice=$notice");
    }else{
       //none were left blank!  We continue...
        if($password != $cpassword) {
             // the passwords are not the same!  
             $notice = "Passwords do not match";
             header("Location: hcaccount.php?notice=$notice");
          }else{
            // the passwords are the same!  we continue...
            //$password = md5($password);
            $password = password_hash("$password", PASSWORD_DEFAULT);
            
            // encrypts the password
            $checkname = mysql_query("SELECT username FROM users WHERE username='$username'");
            $checkname= mysql_num_rows($checkname);
            $checkemail = mysql_query("SELECT email FROM users WHERE email='$email'");
            $checkemail = mysql_num_rows($checkemail);
            if ($checkemail>0|$checkname>0) {
               // oops...someone has already registered with that username or email!
               $notice = "The username or email is already in use";
               header("Location: hcaccount.php?notice=$notice");
            }else{
                // noone is using that email or username!  We continue...
                $username = htmlspecialchars($username);
                $password = htmlspecialchars($password);
                $email = htmlspecialchars($email);

                // the above lines make it so that there is no html in the user submitted information.
                //Everything seems good, lets insert.
                $query = mysql_query("INSERT INTO users (username, password, email,level) VALUES('$username','$password','$email','1')") or die(mysql_error());
                // inserts the information into the database.
                $notice = "The user has been created. You can now login";
                header("Location: hcaccount.php?notice=$notice");
                //echo "You have successfully registered!<br><br><a href="step4.php">Continue Installing</a>";
             }
        }
    }
}

if (isset($_POST['login'])) {
// the form has been submitted.  We continue...
$username = mysql_real_escape_string($_POST['username']);
$typedpass = mysql_real_escape_string($_POST['password']);
// the above lines set variables with the submitted information.  
$info = mysql_query("SELECT * FROM users WHERE username = '$username'") or die(mysql_error());
$data = mysql_fetch_array($info);

$storedpass = $data['password'];
$passwordcheck = password_verify("$typedpass", $storedpass);
//$password = md5(mysql_real_escape_string($_POST['password'].$data['id'])); //salted hash

$prePass = $_POST['password'].$data['id']; //salted hash
$datenumber = date("Y-m-d");
$countfails = mysql_query("SELECT * FROM faillogs WHERE userid = '$data[id]' & datenumber = '$datenumber'") or die(mysql_error());
$countfails = mysql_num_rows($countfails);
if ($countfails > 5){
    //$notice =  "You have exceeded your maximum failed login attempts for today.";   
    $notice = "You have exceeded your maximum failed login attempts for today.";   
    header("Location: hcaccount.php?notice=$notice");
} elseif($passwordcheck === false){
    // the password was not the user's password!
    mysql_query("INSERT INTO faillogs (userid, type, datenumber) VALUES('$data[id]', 'login', '$datenumber' ) ") or die(mysql_error());  
    $notice = "Incorrect username or password!";
    header("Location: hcaccount.php?notice=$notice");
} else{
    // the password was right!
    $query = mysql_query("SELECT * FROM users WHERE username = '$username'") or die(mysql_error());
    $user = mysql_fetch_array($query);
    // gets the user's information

    setcookie("id", $user['id'], time()+21600, '/');  /* expire in 360 minutes */
    setcookie("username", $user['username'], time()+21600, '/');  /* expire in 360 minutes */
    setcookie("pass", $user['password'], time()+21600, '/');  /* expire in 360 minutes */
    // echo ("<meta http-equiv="Refresh" content="0; URL=index.php"/>Thank You! You will be redirected");
    header("Location: hcaccount.php");
    }

}

include 'functions/theme_header.php';

?>

<h3>Login or create a Hostclaw clientarea account</h3>

<?php
 if (empty($notice) && isset($_GET['notice'])) { $notice = $_GET['notice']; }
    if (isset($notice)){
      echo "<div class=\"notice\">
      $notice
      </div>"; 
    }
?>

<div class="offset">

<?php
if (!isset($logged)){ ?>

<p>Create yourself a Hostclaw account to manage your hosting packages.</p>
<p>If you already have account, you can login.</p>
                  <br>
    <table width="400"><tr>
                <td width="200">
                <form method="POST">Username: </td><td width="200"><input type="text" size="15" class="bigtext" maxlength="25" name="username"><br />

                </td>
                </tr><tr>
                <td >Password: </td><td><input type="password" size="15"  class="bigtext" maxlength="25" name="pass"><br />
                </td>
                </tr><tr>
                <td>Confirm Password: </td><td><input type="password"  class="bigtext" size="15" maxlength="25" name="cpass"><br />
                </td>
                </tr><tr>
                <td >Email: </td><td><input type="text" size="15" class="bigtext"  maxlength="100" name="email"><br />

                </td>
                </tr><tr>
                <td valign="top" colspan="2" align="right"><input name="register" type="submit" class="bigbutton"  value="Register"></form>
                </td></tr></table>

<form method="POST">
  <table border="0">
  <tr>
  <td>
  Username: <br><input type="text" size="15" class="bigtext" maxlength="25" name="username">
  </td>
  </tr><tr>
  <td>
  Password: <br><input type="password" size="15" class="bigtext"  maxlength="25" name="password">
  </td>
</tr>
 <tr><td align="right">  <input type="submit" name="login" class="bigbutton"  value="Login"></td></tr>
 
  </table>
</form>

<form method="POST" action="sendpassemail.php">
  <input type="text" name="emailto" class="bigtext" />
  <input type="submit"  name="cake" class="bigbutton" value="Send password email">
</form>

<?php } else {  ?>
    <p>You are logged in. </p>
    <p>Now you can continue to the next part of the order form.</p>
    <br>
    <a href="makeachoice.php">Continue ordering new hosting packages</a>
    <br><br>
    <a href="hcaccount.php?page=logout">Logout</a>

    <p>Below are your hosting packages</p>
    <?php

    $getlist = mysql_query("SELECT * FROM orders WHERE deleted='no' && userid='$logged[id] && orderstatus ='processed' ");
    $num = mysql_num_rows($getlist);

    echo "<TABLE cellpadding=\"5\"><TR>
  <TD WIDTH='30' BGCOLOR='SILVER'>Active?</TD>
  <TD WIDTH='50' BGCOLOR='SILVER'>Username</TD>
  <TD WIDTH='150' BGCOLOR='SILVER'>Domain Name</TD>
  <TD WIDTH='125' BGCOLOR='SILVER'>Package</TD>
  <TD WIDTH='30' BGCOLOR='SILVER'>Expires</TD>
  <TD WIDTH='30' BGCOLOR='SILVER'>Email</TD>
  <TD WIDTH='30' BGCOLOR='SILVER'>Renewal Frequency</TD>
  <!-- <TD WIDTH=30' BGCOLOR='SILVER'>Get Info</TD> -->
  <!-- <TD WIDTH='80' BGCOLOR='SILVER'>Suspend</TD> -->
  </TR>";
    for ($i=0; $i<$num; $i++) {
      $username = mysql_result($getlist,$i,"username");
  $domain = mysql_result($getlist,$i,"domain");
  $package = mysql_result($getlist,$i,"pack");
  $day =  mysql_result($getlist,$i,"day");
  $month =  mysql_result($getlist,$i,"month");
  $year =  mysql_result($getlist,$i,"year");
  $id =  mysql_result($getlist,$i,"id");
  $expires = mysql_result($getlist,$i,"expirydatenumber");
  $paymentfrequency = mysql_result($getlist,$i,"paymentfrequency");
  $orderid = mysql_result($getlist,$i,"id");
  $suspended = mysql_result($getlist,$i,"suspended");
        ?>
        <tr>
<td bgcolor="#<?php echo $bgcolor ?>">
<?php if ($suspended=='yes') { ?><img src="graphics/off.png" width="24" height="24" /><?php } else { ?><img src="graphics/on.png" width="24" height="24" /><?php } ?>
</td>
<td bgcolor="#<?php echo $bgcolor ?>"><?php echo $username ?></td>
<td bgcolor="#<?php echo $bgcolor ?>"><a href="http://<?php echo $domain ?>" target="_blank"><?php echo $domain ?></a></td>
<td bgcolor="#<?php echo $bgcolor ?>"><?php echo $package ?></td>
<td bgcolor="#<?php echo $bgcolor ?>"><?php echo $expires ?></td>
<td bgcolor="#<?php echo $bgcolor ?>"><?php echo $email ?></td>
<td bgcolor="#<?php echo $bgcolor ?>"><?php 
if ($paymentfrequency == "monthly"){ 
  echo "The payment frequency is monthly. <br><a href=\"hcaccount.php?mpfm?=$id\"></a>"; 
}elseif ($paymentfrequency == "monthly"){ 
  echo "The payment frequency is annually. <br><a href=\"hcaccount.php?mpfm?=$id\"></a>"; 
} ?></td>
<!-- <td bgcolor="#<?php echo $bgcolor ?>"><a href="getinfo.php?order=<?php echo $id ?>"><img src="graphics/info.png"></a></td>
<td bgcolor="#<?php echo $bgcolor ?>"><?php if ($suspended=='yes') { ?><img src="graphics/play.png" width="24" height="24" /><?php } else { ?><a href="suspstat.php?stat=suspend&id=<?php echo $orderid ?>"><img src="graphics/pause.png" width="24" height="24" /></a><?php }; ?></td>
-->
</tr>
<?php } ?>
</table>

<?php if(isset($logged)){ ?>
<TABLE cellpadding="5"><TR>
  <TD WIDTH='300' BGCOLOR='SILVER'>Title</TD>
  <TD WIDTH='50' BGCOLOR='SILVER'>Closed</TD>
  </TR>

<?php
$getlist = mysql_query("SELECT * FROM tickets WHERE aboutuserid='$logged[id]'  GROUP BY pond ORDER BY id DESC");
while ($ticket = mysql_fetch_array($getlist)){
  echo "
  <tr>
  <td><A href=\"ticket.php?pond=$ticket[pond]\">$ticket[title]</td>
  <td>"; if ($ticket['closed'] == "yes"){ echo "closed"; } else { echo "open"; } echo "</td>
  </tr>";
}
?>
</TABLE>

<?php } ?>

<h1>Create a new support ticket</h1>
<form method="POST">
  Title: <br><input type="text" name="title" class="bigtext required">
  <br>
  Message<br><textarea name="message" class="bigtext" style="width: 400px; height: 200px;"></textarea>
  <br><br><input type="submit" name="newticket" value="New support ticket" class="bigbutton">
</form>
<?php } ?>


<?php
include 'functions/theme_footer.php';
?>