<?php
//include the whm class file.
require_once('whm.php');

// create a new instance of whm class
$whm= new whm;

//initilize the whm object 
//you can use you hostname or an IP below 
//you can find you whm hash when yopu login into your whm account clickong on "Setup Remote Access Key" link.
$whm->init('hostingz.org','hostingz','8a22a0a335b298d5c0a11d67dc59da91
79e6c551b3e66b96516ee45704f1f861
3366b740792045b52ad61f4a0b0783cc
55a772b11e174593149eb0cb8ada7902
9593a396a94b60e103855c02959ee2fb
051ba9c762bc1ba4aa199d0cf69a660e
b90eeb9ca708d934f3c5f7f498f76178
c957aea98842e35ad327d2f65ccd6804
c95c8d7bcab9b5864aa84c738fd330a7
b0e1e8968f56bea52d79aebb7714d88d
ea54706afc01f5dd19a716dcd3a6a2ce
f93302944a22f89e1db728c98487dc38
b86e62e0eb0318ea49e707cb00b13737
c882c4823fe21144a4ece1ddbab138c2
04f9d6bf39f2e88ce09929917b8cf8be
314321000e5f8f93d414db0b7ba52064
4be22eaa7152c5dfb4abcd55bf95e473
b6b26a29efa1aa14c8a16c709c591336
41a90b1059d610ad06488ff4d7560198
12c57a2a679057c8754e83a6576058d5
be14bfd3753e75295a0193ba3410e630
358f2a408d97df02c7e25b6c5b2ef78c
83cec73158eec191cad0b51d7f3dfa0b
ff8c825facccf02ed66aa5f506340d58
fb8e60a8c6583c29b6adfb849872bcb8
5cecda55bcef214c28898ed6eb52bd8f
1ce7f795855a2f57e719d35d221c1e64
59097bd56d70df579597a2f44310b70e
be9fee45087b958ad41385d0b96bafa9');

//This will output the cpanel/whm version.  
$version= $whm->version();
echo "Cpanel/whm version is: $version <br>";

//This way you can create an account. 
//This function will return a result set as an array in success and will return false on fail.
$result=$whm->createAccount('testdomain.com','testuser','testpassword123','package_test');

//check if creating account was successfull or not.
if($result)
{
    //print the result set
    print_r($result);
}
else
{
    //You can get the errors like this.
    print_r($whm->errors);
}

?> 	
<?php	
/*
 */
		//$this->fp = fsockopen("ssl://" . $this->host, 2087, $errno, $errstr, 30);

		/*
		 * Die on error initializing socket
		 */
		if ($errno == 0 && $this->fp == false)
		{
			$this->errors[]="Socket Error: Could not initialize socket.";
			return false;
		}
		elseif ($this->fp == false)
		{
			$this->errors[]="Socket Error #" . $errno . ": " . $errstr;
			return false;
		}

		/*
		 *  Assemble the header to send
		 */
		$header = "";
		$header .= "GET " . $api_path . " HTTP/1.0\r\n";
		$header .= "Host: " . $this->host . "\r\n";
		$header .= "Connection: Close\r\n";
		$header .= "Authorization: WHM " . $this->user . ":" . $this->accessHash . "\r\n";
		// Comment above line and uncomment below line to use password authentication in place of hash authentication
		//$header .= "Authorization: Basic " . base64_encode($user . ":" . $pass) . "\r\n";
		$header .= "\r\n";

		/*
		 * Send the Header
		 */
		if(!@fputs($this->fp, $header))
		{
			$this->errors[]='Unable to send header.';
			return false;
		}
	// }

	/*
	 * Close the socket
	 */
	function disconnect()
	{
		fclose($this->fp);
	}

	/*
	 * Get the raw output from the server
	 * Output: string
	 */
	function getOutput()
	{
		$rawResult = "";
		while (!feof($this->fp))
		{
			$rawResult .= @fgets($this->fp, 128); // Suppress errors with @
		}


		/*
		 * Ignore headers
		 */
		$rawResultParts = explode("\r\n\r\n",$rawResult);
		$result = $rawResultParts[1];

		/*
		 * Output XML
		 */
		return $result;
	}


	/*
	 * This function lists the verison of cPanel and WHM installed on the server.
	 * Output: string
	 */
	function version()
	{
		//connect using prpoer xml api address
		$this->connect('/xml-api/version');
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		return $xml->version;
	}


	/*
	 * This function lists the server's hostname.
	 * Output: string
	 */
	function gethostname()
	{
		//connect using prpoer xml api address
		$this->connect('/xml-api/gethostname');
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		return $xml->hostname;
	}

	/*
	 * list currently active accounts
	 * Output: array on success, false on fail
	 */
	function listaccts()
	{
		//connect using prpoer xml api address
		$this->connect('/xml-api/listaccts');
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		$xml = new DOMDocument();
		$xml->loadXML($xmlstr);

		// statement block to get the elements of the xml document
		$list = $xml->getElementsByTagName('user');
		$i=0;
		foreach ($list AS $element)
		{
			foreach ($element->childNodes AS $item)
			{
				$result[$i]['user']=$item->nodeValue;
				$i++;
			}
		}

		$list = $xml->getElementsByTagName('domain');
		$i=0;
		foreach ($list AS $element)
		{
			foreach ($element->childNodes AS $item)
			{
				$result[$i]['domain']=$item->nodeValue;
				$i++;
			}
		}

		$list = $xml->getElementsByTagName('plan');
		$i=0;
		foreach ($list AS $element)
		{
			foreach ($element->childNodes AS $item)
			{
				$result[$i]['package']=$item->nodeValue;
				$i++;
			}
		}

		$list = $xml->getElementsByTagName('unix_startdate');
		$i=0;
		foreach ($list AS $element)
		{
			foreach ($element->childNodes AS $item)
			{
				$result[$i]['start_date']=$item->nodeValue;
				$i++;
			}
		}

		//return the result array
		return $result;
	}


	/*
	 * list packages
	 * Output: array on success, false on fail
	 */
	function listPkgs()
	{
		//connect using prpoer xml api address
		$this->connect('/xml-api/listpkgs');
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		$xml = new DOMDocument();
		$xml->loadXML($xmlstr);


		$list = $xml->getElementsByTagName('name');
		$i=0;
		foreach ($list AS $element)
		{
			foreach ($element->childNodes AS $item)
			{
				$result[$i]['package_name']=$item->nodeValue;
				$i++;
			}
		}

		$list = $xml->getElementsByTagName('QUOTA');
		$i=0;
		foreach ($list AS $element)
		{
			foreach ($element->childNodes AS $item)
			{
				$result[$i]['package_quota']=$item->nodeValue;
				$i++;
			}
		}

		$list = $xml->getElementsByTagName('BWLIMIT');
		$i=0;
		foreach ($list AS $element)
		{
			foreach ($element->childNodes AS $item)
			{
				$result[$i]['package_bw_limit']=$item->nodeValue;
				$i++;
			}
		}

		//return the result array
		return $result;
	}

	/*
	 * create a cpanel account
	 * Output: array on success, false on fail
	 */
	function createAccount($acctDomain,$acctUser,$acctPass,$acctPackg)
	{
		//connect using prpoer xml api address
		$this->connect("/xml-api/createacct?username=$acctUser&password=$acctPass&plan=$acctPackg&domain=$acctDomain&ip=n&cgi=y&frontpage=y&cpmod=x3&useregns=0&reseller=0");
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);


		if($xml->result->status==1)
		{
			$result['status']=$xml->result->status;
			$result['statusmsg']=$xml->result->statusmsg;
			$result['ip']=$xml->result->options->ip;
			$result['nameserver']=$xml->result->options->nameserver;
			$result['nameserver2']=$xml->result->options->nameserver2;
			$result['nameserver3']=$xml->result->options->nameserver3;
			$result['nameserver4']=$xml->result->options->nameserver4;
			$result['package']=$xml->result->options->package;
			$result['rawout']=$xml->result->rawout;
			return $result;
		}
		else
		{
			$this->errors[]=$xml->result->statusmsg;
			return false;
		}
	}


	/*
	 * This function displays pertient account information for a specific account.
	 * Output: array on success , false on fail
	 */
	function accountsummary($accUser)
	{
		//connect using prpoer xml api address
		$this->connect("/xml-api/accountsummary?user=$accUser");
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		if($xml->status==1)
		{
			$result['disklimit']=$xml->acct->disklimit;
			$result['diskused']=$xml->acct->diskused;
			$result['diskused']=$xml->acct->diskused;
			$result['domain']=$xml->acct->domain;
			$result['email']=$xml->acct->email;
			$result['ip']=$xml->acct->ip;
			$result['owner']=$xml->acct->owner;
			$result['partition']=$xml->acct->partition;
			$result['plan']=$xml->acct->plan;
			$result['startdate']=$xml->acct->startdate;
			$result['theme']=$xml->acct->theme;
			$result['unix_startdate']=$xml->acct->unix_startdate;
			$result['user']=$xml->acct->user;
			return $result;
		}
		else
		{
			$this->errors[]=$xml->statusmsg;
			return false;
		}
	}

	/*
	 *This function changes the passwd of a domain owner (cPanel) or reseller (WHM) account.
	 * Output: array on success , false on fail
	 */
	function passwd($accUser,$pass)
	{
		//connect using prpoer xml api address
		$this->connect("/xml-api/passwd?user=$accUser&pass=$pass");
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		if($xml->passwd->status==1)
		{
			$result['statusmsg']=$xml->passwd->statusmsg;
			$result['frontpage']=$xml->passwd->frontpage;
			$result['ftp']=$xml->passwd->ftp;
			$result['mail']=$xml->passwd->mail;
			$result['mysql']=$xml->passwd->mysql;
			$result['system']=$xml->passwd->system;
			$result['rawout']=$xml->passwd->rawout;
			return $result;
		}
		else
		{
			$this->errors[]=$xml->passwd->statusmsg;
			return false;
		}
	}

	/*
	 * suspend a cpanel account
	 * Output: string (statusmsg) on success, false on fail
	 */
	function suspend($acctUser,$reason)
	{
		//connect using prpoer xml api address
		$this->connect("/xml-api/suspendacct?user=$acctUser&reason=$reason");
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		if($xml->result->status==1)
		{
			return $xml->result->statusmsg;
		}
		else
		{
			$this->errors[]=$xml->result->statusmsg;
			return false;
		}
	}

	/*
	 * unsuspend a suspended cpanel account
	 * Output: string (statusmsg) on success, false on fail
	 */
	function unsuspend($acctUser)
	{
		//connect using prpoer xml api address
		$this->connect("/xml-api/unsuspendacct?user=$acctUser");
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		if($xml->result->status==1)
		{
			return $xml->result->statusmsg;
		}
		else
		{
			$this->errors[]=$xml->result->statusmsg;
			return false;
		}
	}


	/*
	 * terminate a cpanel account
	 * Output: string (statusmsg) on success, false on fail
	 */
	function terminate($acctUser,$keepDns=0)
	{
		//connect using prpoer xml api address
		$this->connect("/xml-api/removeacct?user=$acctUser&keepdns=$keepDns");
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		if($xml->result->status==1)
		{
			return $xml->result->statusmsg;
		}
		else
		{
			$this->errors[]=$xml->result->statusmsg;
			return false;
		}
	}


	/*
	 * Upgrade/Downgrade and Account (Change Package)
	 * Output: array on success, false on fail
	 */
	function changepackage($accUser,$pkg)
	{
		//connect using prpoer xml api address
		$this->connect("/xml-api/changepackage?user=$accUser&pkg=$pkg");
		//get the output
		$xmlstr=$this->getOutput();
		if($xmlstr=='')
		{
			$this->errors[]='No output.';
			return false;
		}
		//disconnect
		$this->disconnect();

		//get the output xml as an array using simple xml
		$xml = new SimpleXMLElement($xmlstr);

		if($xml->result->status==1)
		{
			$result['statusmsg']=$xml->result->statusmsg;
			$result['rawout']=$xml->result->rawout;
			return $result;
		}
		else
		{
			$this->errors[]=$xml->result->statusmsg;
			return false;
		}
	}

 // }
 ?>