<form action="https://www.moneybookers.com/app/payment.pl" method="post">
  <input type="hidden" name="pay_to_email" value="<?php echo $business ?>"/>
  <input type="hidden" name="return_url" value="[<?php echo $url ?>../functions/paid_moneybookers.php"/> 
  <input type="hidden" name="status_url" value="[<?php echo $url ?>/ordercomplete.php"/> 
  <input type="hidden" name="language" value="EN"/>
  <input type="hidden" name="amount" value="<?php echo $amount ?>"/>
  <input type="hidden" name="currency" value="GBP"/>
  <input type="hidden" name="detail1_description" value="Web hosting at <?php echo $sitename ?>"/>
  <input type="hidden" name="detail1_text" value="Order #<?php echo $orderid ?>"/>
  <input type="hidden" name="transaction_id" value="<?php echo $orderid ?>"/>
  <!-- <input type="hidden" name="logo_url" value="[var.websiteurl]/[var.sitelogopath;ifempty='themes/[var.theme]/graphics/_defaultlogo_[var.themevariation].png']"/> -->
  <input type="submit" value="Checkout"/>
</form>