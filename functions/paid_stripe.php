<?php
ob_start();
session_start();

require("hooks.php"); $Hooks = new Hooks;
error_reporting(E_ALL ^ E_NOTICE); 
if (preg_match("/paid_/", "$_SERVER[PHP_SELF]" )) { require("../functions/config.php"); }

$sitedata = mysql_query("SELECT * FROM settings");

require("stripelibrary.php");
$stripepublishablekey = mysql_result($sitedata,0,"stripepublishablekey");
Stripe::setApiKey("$stripepublishablekey");

$centcost = $amount / 100;
 
try {
    Stripe_Charge::create([
        'amount' => $centcost, // this is in cents: $20
        'currency' => 'usd',
        'card' => $_POST['stripeToken'],
        'description' => "$item_name"
    ]);
    $orderamount = $amount * 100; $orderid = $_POST['orderid'];
    $orderstatus = $Hooks->FetchOrderStatus($orderid);
    /* debug */ if ($debug == "yes") { echo "orderdid is $orderid // The orderstatus is $orderstatus"; }
    if ($orderstatus == "processing"){ $Hooks->ExecuteProcessOrder($orderid, $orderstatus, $_POST['txn_id'], $orderamount); }
    if ($orderstatus == "processed"){ $Hooks->ExecuteRenewOrder($orderid, $orderstatus, $_POST['txn_id'], $orderamount); }
} catch (Stripe_CardError $e) {
    // Declined. Don't process their purchase.
    // Go back, and tell the user to try a new card
}
?>