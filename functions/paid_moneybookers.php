<?php
ob_start();
session_start();
require("hooks.php"); $Hooks = new Hooks;  
error_reporting(E_ALL ^ E_NOTICE); 
if (preg_match("/pay_/", "$_SERVER[PHP_SELF]" )) { require("../config.php"); }
$orderamount = $_POST['mb_amount']; $orderid = $_POST['transaction_id'];
$debug = "yes"; //turn this to yes, to possibly use $_GET[custom] instead of $_POST[custom]
/* debug */ if ($debug == "yes") { if (empty($_POST['transaction_id'])) {$orderid = $_GET['transaction_id']; } echo "debug mode is on<br>"; }

require_once 'KLogger.php'; $log = new KLogger ( "log.txt" , KLogger::DEBUG );

//////////////////////////////////////
// Read the post from Moneybookers

// Validate the Moneybookers signature
$concatFields = $_POST['merchant_id']
    .$_POST['transaction_id']
    .strtoupper(md5('mercury'))
    .$_POST['mb_amount']
    .$_POST['mb_currency']
    .$_POST['status'];

$MBEmail = $_POST['pay_to_email'];

// Ensure the signature is valid, the status code == 2,
// and that the money is going to you
if (strtoupper(md5($concatFields)) == $_POST['md5sig']
    && $_POST['status'] == 2
    && $_POST['pay_to_email'] == $MBEmail)
{
    // Valid transaction.
    $payment = "success";

    //TODO: generate the product keys and
    //      send them to your customer.
    $orderstatus = mysql_fetch_array(mysql_query("SELECT * FROM orders WHERE id='$orderid' "));
    $orderstatus = $orderstatus[status];
    if ($orderstatus == "processing"){ //processed orders do not process again
        $Hooks->ExecuteProcessOrder($orderid, $orderstatus);
        $orderstatus = $Hooks->FetchOrderStatus($orderid);
        /* debug */ if ($debug == "yes") { echo "orderdid is $orderid // The orderstatus is $orderstatus"; }
        if ($orderstatus == "processing"){ $Hooks->ExecuteProcessOrder($orderid, $orderstatus, $_POST['transaction_id'], $_POST['mb_amount']); }
        if ($orderstatus == "processed"){ $Hooks->ExecuteRenewOrder($orderid, $orderstatus, $_POST['transaction_id'], $_POST['mb_amount']); }
    }
    ////_end
}
elseif(empty($_POST)){
    $payment = "unnecessary";
    if ($debug == "yes") { echo "<h1>There is no transaction taking place.</h1>"; }
}
else
{
    // Invalid transaction. Bail out
    echo "<h1>There has been an error with the validation process</h1>Code: $_POST[failed_reason_code]";
    $payment = "fail"; 

    $postback = implode("", $_POST)
    $Hooks->EmailFailedOrder($orderid, "moneybookers/skrill", $_POST['transaction_id'], "$postback");
    exit;
}