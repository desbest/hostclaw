<?php
// Paymentwall PHP Library: https://www.paymentwall.com/lib/php
require_once('paymentwall.php');
Paymentwall_Config::getInstance()->set(array(
    'api_type' => Paymentwall_Config::API_GOODS,
    'public_key' => $paymentwallPublicKey,
    'private_key' => $paymentwallPrivateKey
));

$widget = new Paymentwall_Widget(
    "$userid",
    'p10',
    array(
        new Paymentwall_Product(
            "$packageid",                           
            "$amount",
            "$currencycode",                                  
            "$showname",                      
            Paymentwall_Product::TYPE_FIXED
        )
    ),
    array('email' => "$email", 'orderid' => "$orderid", 'grossamount' => "$amount", 'customer[username]' => "$_SESSION[username]", 'customer[address]' => "$_SESSION[address]", 'ag_type' => "fixed", 'success_redirect_url' => "$siteurl/complete.php?status=success")
    );
echo $widget->getHtmlCode();
?>