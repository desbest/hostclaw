<?php
$result = mysql_query("SELECT * FROM settings LIMIT 1");
$stripepublishablekey = mysql_result($result,0,"stripepublishablekey");

// Set your API key


?>
<script src="//js.stripe.com/v2/"></script>
<script>
    (function() {
        Stripe.setPublishableKey('<?php echo $stripepublishablekey ?>');
        // Event Listeners
		$('#payment-form').on('submit', generateToken);
		 
		var generateToken = function(e) {
		    var form = $(this);
		 
		    // No pressing the buy now button more than once
		    form.find('button').prop('disabled', true);
		 
		    // Create the token, based on the form object
		    Stripe.create(form, stripeResponseHandler);
		 
		    // Prevent the form from submitting
		    e.preventDefault();
		};
		 
		var stripeResponseHandler = function(status, response) {
		    var form = $('#payment-form');
		 
		    // Any validation errors?
		    if (response.error) {
		        // Show the user what they did wrong
		        form.find('.payment-errors').text(response.error.message);
		 
		        // Make the submit clickable again
		        form.find('button').prop('disabled', false);
		    } else {
		        // Otherwise, we're good to go! Submit the form.
		 
		        // Insert the unique token into the form
		        $('<input>', {
		            'type': 'hidden',
		            'name': 'stripeToken',
		            'value': response.id
		        }).appendTo(form);
		 
		        // Call the native submit method on the form
		        // to keep the submission from being canceled
		        form.get(0).submit();
		    }
		};
    })();
</script>
<form action="complete.php" method="POST">
  <script
    src="https://checkout.stripe.com/v2/checkout.js" class="stripe-button"
    data-key="<?php echo $stripepublishablekey ?>"
    data-amount="<?php echo $amount ?>"
    data-name="<?php echo $sitename ?>"
    data-description="<?php echo $item_name ?>"
    data-image="">
  </script>
  <input type="hidden" name="orderid" value="<?php echo $orderid ?>">
</form>