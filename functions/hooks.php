<?php

class Hooks {
    /* Contents
        ----> SendTicketEmail
        ----> EmailFailedOrder
        ----> SendPasswordResetEmail
        ----> SendFreeCreateEmail
        ----> SendFreeUpgradeEmail
        ----> SendRenewalEmail
        ----> FetchOrderStatus
        ----> ExecuteRenewOrder
        ----> ExecuteProcessOrder
    */

    function SendAutoClosedTicketEmail(){
        $user = mysql_query("SELECT * FROM users
        WHERE id='$recipuserid' LIMIT 1") or die(mysql_error());
        $user = mysql_fetch_array($user);

        $conf = mysql_query("SELECT * FROM settings");
        $sitename = mysql_result($conf,0,"sitename");
        $siteurl = mysql_result($conf,0,"siteurl");
        $domainname = str_replace("http://", "", "$siteurl");
        $domainchunks = explode("/", $domainname);  
        $domainname = $domainchunks[0];

        $to      = "$user[email]";
        $subject = "$sitename --> Ticket has bee closed";
        $message = "This is $sitename. \n";
        $message .= "Your ticket has been closed because there has been no response from you in 48 hours. Simply reply to your ticket to open it again. \n";
        $message .= "<br><br><a href=\"$siteurl/hcaccount.php\">View the response</a> \n";
        $headers  = "From: $sitename <donotreply@$domainname>" . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        "Reply-To: donotreply@$domainname" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
        // $datenumber = date('Y-m-d');
    }

    function SendTicketEmail($pond, $recipuserid){
        $user = mysql_query("SELECT * FROM users
        WHERE id='$recipuserid' LIMIT 1") or die(mysql_error());
        $user = mysql_fetch_array($user);

        $conf = mysql_query("SELECT * FROM settings");
        $sitename = mysql_result($conf,0,"sitename");
        $siteurl = mysql_result($conf,0,"siteurl");
        $domainname = str_replace("http://", "", "$siteurl");
        $domainchunks = explode("/", $domainname);  
        $domainname = $domainchunks[0];

        $to      = "$user[email]";
        $subject = "$sitename --> New ticket reply";
        $message = "This is $sitename. \n";
        $message .= "There has been a reply to your ticket. \n";
        $message .= "<br><br><a href=\"$siteurl/hcaccount.php\">View the response</a> \n";
        $headers  = "From: $sitename <donotreply@$domainname>" . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        "Reply-To: donotreply@$domainname" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
        // $datenumber = date('Y-m-d');

    }

    function EmailFailedOrder($orderid, $paymentprocessor, $transactionid, $postback){
        $result = mysql_query("SELECT * FROM users
        WHERE level='5' LIMIT 1") or die(mysql_error());
        $resultrows = mysql_num_rows($result);
        $resultarray = mysql_fetch_array($result);

        $conf = mysql_query("SELECT * FROM settings");
        $sitename = mysql_result($conf,0,"sitename");
        $siteurl = mysql_result($conf,0,"siteurl");
        $domainname = str_replace("http://", "", "$siteurl");
        $domainchunks = explode("/", $domainname);  
        $domainname = $domainchunks[0];



        $to      = "$resultarray[email]";
        $subject = "$sitename --> Failed payment";
        $message = "This is $sitename. \n";
        $message .= "There has been a failed payment. \n";
        $message .= "<b>Order id: </b>$orderid. \n";
        $message .= "<b>Payment Processor </b>$paymentprocessor. \n";
        $message .= "<b>Payment Processor </b>$transactionid. \n";
        $message .= "<b>Postback </b>$postback. \n";
        $headers  = "From: $sitename <donotreply@$domainname>" . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        "Reply-To: donotreply@$domainname" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
        $datenumber = date('Y-m-d');
    }

    function SendPasswordResetEmail($email, $random){
        $result = mysql_query("SELECT * FROM users
        WHERE email='$whotosend'") or die(mysql_error());
        $resultrows = mysql_num_rows($result);
        $resultarray = mysql_fetch_array($result);

        $conf = mysql_query("SELECT * FROM settings");
        $sitename = mysql_result($conf,0,"sitename");
        $siteurl = mysql_result($conf,0,"siteurl");
        $domainname = str_replace("http://", "", "$siteurl");
        $domainchunks = explode("/", $domainname);  
        $domainname = $domainchunks[0];

        $to      = "$resultarray[email]";
        $subject = "$sitename --> Your password";
        $message = "This is $sitename. \n Go to the link below to give yourself, $resultarray[username] a new password. \n $siteurl/admin/resetpass.php?accesscode=$random";
        $headers  = "From: $sitename <donotreply@$domainname>" . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        "Reply-To: donotreply@$domainname" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
        $datenumber = date('Y-m-d');
    }

    function SendFreeCreateEmail($orderid){
        //$to = mysql_result($get_userdata,0,"email");
        $get_datafields = mysql_query("SELECT * FROM orders WHERE id='$orderid'");
        $to = mysql_result($get_datafields,0,"email");

        $from = $sitename."<".$sitemail.">";

        $subject = "Hosting Account Details";
        $body = file_get_contents("theme_newaccountemail.php");
        $body = addslashes($body);
        
        $sendas = "donotreply@$domainname";
        $headers = "From: $sitename <$sendas>" . "\r\n";
        /**/$headers  = 'MIME-Version: 1.0' . "\r\n";
        /**/$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        
        mail("$to","$subject","$body","$headers");
    }

    function SendFreeUpgradeEmail($orderid){
        //$to = mysql_result($get_userdata,0,"email");
        $get_datafields = mysql_query("SELECT * FROM orders WHERE id='$orderid'");
        $to = mysql_result($get_datafields,0,"email");

        $from = $sitename."<".$sitemail.">";

        $subject = "Hosting Account Details";
        $body = file_get_contents("theme_newaccountemail.php");
        $body = addslashes($body);
        
        $sendas = "donotreply@$domainname";
        $headers = "From: $sitename <$sendas>" . "\r\n";
        /**/$headers  = 'MIME-Version: 1.0' . "\r\n";
        /**/$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        
        mail("$to","$subject","$body","$headers");
    }
    
    function SendRenewalEmail($orderid){
        
        $get_datafields = @mysql_query("SELECT * FROM orders WHERE id='$orderid'");          
        $acctdomain = @mysql_result($get_datafields,0,"domain");      
        $acctplan = @mysql_result($get_datafields,0,"pack"); 
        $acctplan = explode("_", $acctplan); $acctplan = $acctplan[1];
        $acctuser = @mysql_result($get_datafields,0,"username");
        $acctpass = @mysql_result($get_datafields,0,"password");
        $lookmeup = @mysql_result($get_datafields,0,"userid");
        $orderid = @mysql_result($get_datafields,0,"id");

        $get_sitedata = mysql_query("SELECT * FROM settings");
        $sitename = mysql_result($get_sitedata,0,"sitename");      
        $siteurl = mysql_result($get_sitedata,0,"siteurl");
        
        ///////////////////////////////////////
        //Email the account details
        ///////////////////////////////////////
        $get_userdata = @mysql_query("SELECT * FROM users WHERE id='$lookmeup'");      
        $to = @mysql_result($get_userdata,0,"email");  
        $userid =  @mysql_result($get_userdata,0,"id");  
        $name =  @mysql_result($get_userdata,0,"name");  
        $from = $sitename."<".$sitemail.">";      //$to = "afaninthehouse@gmail.com";    
        $subject = "Renew your hosting account";
        //echo "<h1>order: $customerID cust: $userid</h1>"; exit();
        $body = file_get_contents("theme_renewalemail.php");
        //echo "<h1>$body</h1>";    exit();
        //$body = addslashes($body);      
        $sendas = "donotreply@hostingz.org";      
        $headers      = "From: Hostingz <$sendas>" . "\r\n";      
        $headers .= 'MIME-Version: 1.0' . "\r\n";      
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";      
        $task1 = mail($to,$subject,$body,$headers);
        //$task1 = mail("afaninthehouse@gmail.com",$subject,$body,$headers);
        if ($task1){ return true; } else { return false; }

    }
    
    function FetchOrderStatus($orderid){
        $orderstatus = mysql_fetch_array(mysql_query("SELECT * FROM orders WHERE id='$orderid' "));
        return $orderstatus['orderstatus'];
    }
    
    function ExecuteRenewOrder($orderid, $orderstatus, $transactionid, $amountpaid){
        require("whm.php"); 
        //require("../includes/config.php.inc");
        require(__DIR__ . "/../functions/config.php"); //for the cron 
        $whm= new whm;
        $whm->init("$whmhost","$whmuser","$whmaccesshash");
        $version= $whm->version(); 
        //echo "<br>Cpanel/whm version is: $version <br>"
        
        $getorder = mysql_query("SELECT * FROM orders WHERE id='$orderid'");
        $username = mysql_result($getorder,$i,"username");
        $userid = @mysql_result($getorder,0,"userid");
        
        $task2=$whm->unsuspend($username);
        
        $datenumber = date("Y-m-d");
        $oldExpiryDate = mysql_fetch_array(mysql_query("SELECT * FROM orders WHERE id='$orderid' "));
        $oldExpiryDate = $oldExpiryDate['expirydatenumber'];
        $thirtyonedaysafter = strtotime ( '+31 day' , strtotime ( $oldExpiryDate ) ) ;
        $thirtyonedaysafter = date ( 'Y-m-d' , $thirtyonedaysafter );

        $shake = mysql_query("UPDATE orders SET expirydatenumber='$thirtyonedaysafter' WHERE id='$orderid' ");
        $make = mysql_query("INSERT INTO invoices 
        (userid, orderid, transno, amount, referrer, datenumber) VALUES('$userid', '$orderid', '$transactionid', '$amountpaid', '$SESSION_[ahmreferrer]','$datenumber' ) ") 
        or die(mysql_error());  
        
    }
    
    function ExecuteProcessOrder($orderid, $orderstatus, $transactionid, $amountpaid){
        require("whm.php"); 
        require("../includes/config.php.inc"); 
        $whm= new whm;
        $whm->init("$whmhost","$whmuser","$whmaccesshash");
        $version= $whm->version(); 
        //echo "<br>Cpanel/whm version is: $version <br>";
        
        $get_sitedata = mysql_query("SELECT * FROM settings");
        $sitename = mysql_result($get_sitedata,0,"sitename");      
        $siteurl = mysql_result($get_sitedata,0,"siteurl");  
        $conversions = addslashes(mysql_result($get_sitedata,0,"conversions"));
        $domainname = str_replace("http://", "", "$siteurl");
        $domainchunks = explode("/", $domainname);  
        $domainname = $domainchunks[0];    
        $sitemail = mysql_result($get_sitedata,0,"email");  

        $get_datafields = @mysql_query("SELECT * FROM orders WHERE id='$orderid'");          
        $acctdomain = @mysql_result($get_datafields,0,"domain");      
        $acctplan = @mysql_result($get_datafields,0,"pack"); 
        $technicalplan = $acctplan;
        $acctplan = explode("_", $acctplan); $acctplan = $acctplan[1];
        $acctuser = @mysql_result($get_datafields,0,"username");
        $acctpass = @mysql_result($get_datafields,0,"password");
        $lookmeup = @mysql_result($get_datafields,0,"userid");

        ///////////////////////////////////////
        //Email the account details
        ///////////////////////////////////////
        $get_userdata = @mysql_query("SELECT * FROM users WHERE id='$lookmeup'");      
        $to = @mysql_result($get_userdata,0,"email");  
        $userid =  @mysql_result($get_userdata,0,"id");  
        $from = $sitename."<".$sitemail.">";      //$to = "afaninthehouse@gmail.com";    
        $subject = "Hosting Account Details";
        //echo "<h1>order: $customerID cust: $userid</h1>"; exit();
        $body = file_get_contents("theme_newaccountemail.php");
        //echo "<h1>$body</h1>";    exit();
        //$body = addslashes($body);      
        $sendas = "donotreply@hostingz.org";      
        $headers  = "From: Hostingz <$sendas>" . "\r\n";      
        $headers .= 'MIME-Version: 1.0' . "\r\n";      
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";      
        $task1 = mail($to,$subject,$body,$headers);      
        
        ///////////////////////////////////////
        //Create the hosting account
        ///////////////////////////////////////
        $acctDomain = $acctdomain;
        $acctUser = $acctuser;
        $acctPass = $acctpass;
        $acctEmail = $to;
        $acctPackg = $technicalplan;

        $task2=$whm->createAccount($acctDomain,$acctUser,$acctPass,$acctPackg,$acctEmail);
        //$task2=$whm->createAccount('testdomain.com','testuser','changeme','testpassword123','package_test'); //debug mode
        if ($task2) { /* print_r($task2); */ } else { echo "<br><strong>Error whilst creating your hosting account. Forward to technical support.</strong><br>"; print_r($task2->errors); echo "<hr>"; } 

        ///////////////////////////////////////
        //Print the conversions html
        ///////////////////////////////////////
        $conversions addslashes($conversions);
        echo "$conversions";

        ///////////////////////////////////////
        //Insert the row for the transaction
        ///////////////////////////////////////
        $datenumber = date("Y-m-d");
        $task3 = mysql_query("INSERT INTO invoices 
        (userid, orderid, transno, amount, referrer, datenumber) VALUES('$userid', '$orderid', '$transactionid', '$amountpaid', '$SESSION_[ahmreferrer]','$datenumber' ) ") 
        or die(mysql_error());  
        
        ///////////////////////////////////////
        //Conversions
        ///////////////////////////////////////
            //conversions goes here echo "";
            $USDtoGBP = round(($amountpaid / 1.52), 2);
            //echo "<img src=\"https://aflite.co.uk/track/goal?mid=32448&goalid=1167&ref=$orderid&value=$USDtoGBP\" width=\"1\" height=\"1\"/>";
        
        ///////////////////////////////////////
        //Class the order as processed
        ///////////////////////////////////////
        $datenumber = date("Y-m-d");
        $twentyeightdaysafter = strtotime ( '+28 day' , strtotime ( $datenumber ) ) ;
        $twentyeightdaysafter = date ( 'Y-m-d' , $twentyeightdaysafter );

        $task4 = mysql_query("UPDATE orders SET active='yes', orderstatus='processed', signupdatenumber= '$datenumber', expirydatenumber='$twentyeightdaysafter' WHERE id='$orderid'") or die(mysql_error());  
        
        if ($task1 == true && $task2 == true && $task3 == true && $task4){ return true; } else { return false; }
    }
    
}
?>