<?php
//http://pastie.org/private/8xy4jnkfxznwyhbsnouiq
//http://forums.cpanel.net/members/meglio/
	/**
	 * Checks if username taken.
	 * This method must be used instead of usernamTaken
	 * @param string $username Username to check
	 * @return bool True if username taken already, otherwise false
	 */
	function usernameTakenXml($username)
	{
		$result = $this->call('listaccts', array(
			'searchtype' => 'user',
			'search' => $username
		), 'xml');
		return (isset($result->acct));
	}
	
	/**
	 * Generates CPanel username of 6 chars length
	 * @param string $baseUsername Original username to be based on
	 * @return string Free to use CPanel username
	 */
	function inventUsername($baseUsername = false)
	{
		$this->CI->load->helper('string');
		
		// If not username specified - invent one
		if (!$baseUsername)
			$baseUsername = 'u' . random_string('alnum', 3);
		
		// ...otherwise limit it to 3 symbols
		else
		{
			// Remove everything except letters & numbers
			$baseUsername = preg_replace('/[^a-zA-Z0-9\s]/', '', $baseUsername);
			// Remove numbers at beginning of the name
			$baseUsername = preg_replace('/^\d+/', '', $baseUsername);
			// Get only first 4 symbols
			$baseUsername = substr($baseUsername, 0, 4);
			// If everything was numbers, we must still invent few random numbers
			if (strlen($baseUsername) == 0)
				$baseUsername = 'u' . random_string('alnum', 3);
		}
		$baseUsername = strtolower($baseUsername);
		
		// Some usernames cannot be used
		if (in_array($baseUsername, array('mail')))
			$baseUsername[strlen($baseUsername)-1] = random_string('nozero', 1);
		
		// Find free username
		$userName = $baseUsername;
		while (true)
		{
			usleep(1000); // very small delay to avoid ddos to CPanel
			if (!$this->usernameTakenXml($userName))
				break;
			$userName = $baseUsername . random_string('nozero', 2);
		}
		return $userName;
	}
?>