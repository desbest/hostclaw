<?php
ob_start();
session_start();
require("hooks.php"); $Hooks = new Hooks;  
error_reporting(E_ALL ^ E_NOTICE); 
if (preg_match("/paid_/", "$_SERVER[PHP_SELF]" )) { require("../functions/config.php"); }
$orderamount = $_POST[payment_gross]; $orderid = $_POST['custom']; 
$debug = "nb"; //turn this to yes, to possibly use $_GET[custom] instead of $_POST[custom]
/* debug */ if ($debug == "yes") { if (empty($_POST[custom])) {$orderid = $_GET[custom]; } echo "debug mode is on<br>"; }


//////////////////////////////////////
// Read the post from PayPal and add 'cmd' 
$req = 'cmd=_notify-validate'; 
if(function_exists('get_magic_quotes_gpc')) {
  $get_magic_quotes_exits = true;
} 
//////////////////////////////////////
// Handle escape characters, which depends on setting of magic quotes
foreach ($_POST as $key => $value)  {
  if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {  
    $value = urlencode(stripslashes($value)); 
  } else { 
    $value = urlencode($value); 
  }  
  $req .= "&$key=$value";  
}
//////////////////////////////////////
// Post back to PayPal to validate 
$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n"; 
$header .= "Content-Type: application/x-www-form-urlencoded\r\n"; 
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n"; 
$fp = fsockopen ('www.sandbox.paypal.com', 80, $errno, $errstr, 30); 

//////////////////////////////////////
// generate the post string from the _POST vars aswell as load the
// _POST vars into an arry so we can play with them from the calling
// script.
$post_string = '';    
foreach ($_POST as $field=>$value) { 
	//echo "<b>Field:</b> $field <b>Value:</b> $value<br>";
	$post_string .= $field.'='.urlencode(stripslashes($value)).'&'; 
}
$post_string.="cmd=_notify-validate"; // append ipn command

//////////////////////////////////////
// Process validation from PayPal 
if (!$fp) { // HTTP ERROR  
  echo "<h1>There has been an error with the validation process</h1>";
} else { 
// NO HTTP ERROR  
  fputs ($fp, $header . $req); $fRun = "1";
  while (!feof($fp)) { 
    $res = fgets ($fp, 1024); 
    
	if (!isset($_POST['payment_status'])) { $payment = "unnecessary"; }
	elseif ($_POST['payment_status'] == "Completed"){ $payment = "success"; }
    else { $payment = "fail"; 
           $postback = implode("", $_POST)
           $Hooks->EmailFailedOrder($orderid, "paypal sandbox", $_POST['txn_id'], "$postback");
  } 
    if ($fRun < 2){ //check this
        /* debug */ if ($debug == "yes") { echo "<strong>payment is: </strong>$payment<br>"; }
        
        if ($payment == "success" || $debug == "yes"){
            $orderstatus = $Hooks->FetchOrderStatus($_GET['custom']);
            /* debug */ if ($debug == "yes") { echo "orderdid is $orderid // The orderstatus is $orderstatus"; }
            if ($orderstatus == "processing"){ $Hooks->ExecuteProcessOrder($orderid, $orderstatus); }
            if ($orderstatus == "processed"){ $Hooks->ExecuteRenewOrder($orderid, $orderstatus, $_POST['txn_id'], $_POST['payment_gross']); }
        }   
        //////////////////////////
  $fRun ++;
  } } //close fRun
  }
  fclose ($fp); 
?>