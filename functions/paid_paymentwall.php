<?php
ob_start();
session_start();
require("hooks.php"); $Hooks = new Hooks;  
error_reporting(E_ALL ^ E_NOTICE); 
if (preg_match("/paid_/", "$_SERVER[PHP_SELF]" )) { require("../functions/config.php"); }
require_once 'KLogger.php'; $log = new KLogger ( "log.txt" , KLogger::DEBUG );
//$log->LogInfo('We are inside the IPN php file!');

// Paymentwall PHP Library: https://www.paymentwall.com/lib/php
require_once('paymentwall.php');
$pingback = new Paymentwall_Pingback($_GET, $_SERVER['REMOTE_ADDR']);
Paymentwall_Config::getInstance()->set(array(
    'api_type' => Paymentwall_Config::API_GOODS,
    'public_key' => '1c41b58203e5f760ceadcecbfbceb198',
    'private_key' => '71a707dc8358121d29a5755e05ee3c1b'
));
if ($pingback->validate()) {
    $productId = $pingback->getProduct()->getId();
    $orderid = $pingback->getParameter("orderid");
    $transactionid = $pingback->getPingbackUniqueId();
    $grossamount = $pingback->getParameter("grossamount");
    if ($pingback->isDeliverable()) {
        //$log->LogInfo("run the hooks");
        $orderstatus = $Hooks->FetchOrderStatus($orderid);
        if ($orderstatus == "processing"){ $Hooks->ExecuteProcessOrder($orderid, $orderstatus, $transactionid, $grossamount); }
        if ($orderstatus == "processed"){ $Hooks->ExecuteRenewOrder($orderid, $orderstatus, $transactionid, $grossamount); }
    } else if ($pingback->isCancelable()) {
         //withdraw the product
    } 
    echo 'OK';
} else {
    echo $pingback->getErrorSummary();
    $errors = $pingback->getErrorSummary();
    $log->LogInfo($errors);

    $postback = implode("", $_POST)
    $Hooks->EmailFailedOrder($orderid, "paymentwall", $transactionid, "$errors");
}

?>