<?php
require '../functions/versionnumber.php';
?>
<html>
<head>
<title>Hostclaw Installer</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li><a>Step 1</a></li>
<li><a>Step 2</a></li>
<li><a>Step 3</a></li>
<li id="active"><a id="current"><a>Step 4</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Install Hostclaw v<?=$versionnumber;?></font><br>	
		


Your Hostclaw is now finished installing.
<br>You should now start making an ftp connection to this website.
<br><br><br>
<BR><img src="warning.png" align="left" width="48" height="48" ><font color="red">IMPORTANT:</font> Edit <b>functions/config.php</b> with the same database values that <b>includes/config.inc.php</b> had.
<br><br><br>
<br><img src="warning.png" align="left" width="48" height="48" ><font color="red">IMPORTANT:</font> Don't forget to edit the files in functions/ that begin with <b>theme_</b> as this is what your clients will see when they get a hosting account. Give the email branding.
<br><img src="warning.png" align="left" width="48" height="48" ><font color="red">IMPORTANT:</font> Create a cron job for <b>functions/cron_renewal.php</b> and <b>functions/cron_closetickets.php</b>.
<br><br><br>
<br><img src="information.png" width="48" height="48" align="left"><font color="red">Notice: </font><b>Once you have finished the two tasks above.</b>
You can delete the install/ directory if you want even though it doesn't allow modification to the database any more.


<br><br>

Thank you for choosing Hostclaw
<br><br>
<form action="../admin/"><input type=submit class="bigbutton" value="Admin Panel"></form>


</div>
</body>
</html>
