-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 23, 2020 at 07:07 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET time_zone = "+00:00";

--
-- Database: `hostclaw`
--

-- --------------------------------------------------------

--
-- Table structure for table `affiliates`
--

CREATE TABLE `affiliates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `msn` varchar(250) NOT NULL DEFAULT 'Not Specified',
  `aim` varchar(250) NOT NULL DEFAULT 'Not Specified',
  `location` varchar(36) NOT NULL DEFAULT 'Not Specified',
  `balance` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `faillogs`
--

CREATE TABLE `faillogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `datenumber` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forumscripts`
--

CREATE TABLE `forumscripts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `forumname` varchar(255) NOT NULL,
  `confpage` varchar(255) NOT NULL,
  `enabled` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `userid` smallint(6) NOT NULL,
  `orderid` smallint(6) NOT NULL,
  `transno` varchar(50) NOT NULL,
  `amount` varchar(8) NOT NULL,
  `referrer` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `userid` smallint(6) NOT NULL,
  `email` varchar(6) NOT NULL,
  `hostingaccountusername` varchar(10) NOT NULL,
  `forumusername` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `websitename` varchar(100) NOT NULL,
  `websitedescription` varchar(100) NOT NULL,
  `pack` varchar(100) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `day` varchar(30) NOT NULL,
  `month` varchar(30) NOT NULL,
  `year` varchar(5) NOT NULL,
  `suspended` varchar(20) DEFAULT NULL,
  `startupposts` int(11) NOT NULL,
  `monthlyposts` int(11) NOT NULL,
  `accepted` varchar(255) NOT NULL DEFAULT 'no',
  `active` varchar(255) NOT NULL DEFAULT 'no',
  `orderstatus` varchar(255) NOT NULL DEFAULT 'processing',
  `signupdatenumber` varchar(255) NOT NULL,
  `expirydatenumber` varchar(255) NOT NULL,
  `renewaldatenumber` varchar(255) NOT NULL,
  `paymentfrequency` varchar(255) NOT NULL,
  `freeorpaid` varchar(255) NOT NULL,
  `referrer` varchar(255) NOT NULL,
  `deleted` varchar(255) NOT NULL DEFAULT 'no',
  `upgradepending` varchar(255) NOT NULL,
  `upgradepackage` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `paidpackages`
--

CREATE TABLE `packages` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `cpaneltitle` varchar(100) NOT NULL,
  `displaytitle` varchar(100) NOT NULL,
  `cost` float NOT NULL,
  `shown` varchar(10) NOT NULL DEFAULT 'yes',
  `startupposts` varchar(10) NOT NULL,
  `monthlyposts` varchar(10) NOT NULL,
  `datenumber` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `paymentprocessors`
--

CREATE TABLE `paymentprocessors` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `payname` varchar(100) NOT NULL,
  `confpage` varchar(100) NOT NULL,
  `enabled` varchar(4) NOT NULL,
  `datenumber` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Insert data into `paymentprocessors`
--

INSERT INTO `paymentprocessors` (`id`, `payname`, `confpage`, `enabled`) VALUES
(1, 'paypal', 'pay_paypal.php', 'no'),
(2, 'paypalsandbox', 'pay_paypalsandbox.php', 'no'),
(3, 'moneybookers', 'pay_moneybookers.php', 'no'),
(4, 'stripe', 'pay_stripe.php', 'no'),
(5, 'paymentwall', 'pay_paymentwall.php', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `resets`
--

CREATE TABLE `resets` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `userid` varchar(100) NOT NULL,
  `datenumber` varchar(25) NOT NULL,
  `accesscode` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitename` varchar(100) NOT NULL,
  `siteurl` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `paypalemail` varchar(100) NOT NULL,
  `paypalsandboxemail` varchar(255) NOT NULL,
  `moneybookersemail` varchar(255) NOT NULL,
  `paymentwallpublickey` varchar(255) NOT NULL,
  `paymentwallprivatekey` varchar(255) NOT NULL,
  `stripepublickey` varchar(255) NOT NULL,
  `stripeprivatekey` varchar(255) NOT NULL,
  `conversions` text NOT NULL,
  `paypalemail` varchar(255) NOT NULL,
  `currencycode` varchar(4) NOT NULL,
  `cron` tinyint(4) NOT NULL,
  `autosus` tinyint(4) NOT NULL,
  `forumscript` varchar(255) NOT NULL,
  `forumpath` varchar(255) NOT NULL,
  `panelused` varchar(255) NOT NULL,
  `fusertable` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Insert data into `settings`
--

INSERT INTO `settings` (`id`, `sitename`, `siteurl`, `email`, `paypalemail`, `paypalsandboxemail`, `moneybookersemail`, `paymentwallpublickey`, `paymentwallprivatekey`, `stripepublickey`, `stripeprivatekey`, `conversions`, `currencycode`, `cron`, `autosus`, `forumscript`, `forumpath`, `panelused`, `fusertable`, `datenumber`) 
VALUES (NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '','')

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `address` text,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `forumusername` varchar(255) DEFAULT NULL,
  `hostingaccountusername` varchar(255) DEFAULT NULL,
  `freeorpaid` varchar(255) NOT NULL,
  `referrer` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `signupdatenumber` varchar(255) NOT NULL,
  `msn` varchar(100) NOT NULL,
  `aim` varchar(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `level` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;