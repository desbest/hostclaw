# Hostclaw

Billing script for WHM that is based on Auto Host Manager.

Screenshots coming soon.

Check functions/versionnumber.php for the version number or change the tag on github/gitlab.

## Features

    * Support for free hosting (good for free web hosts)
    * Checks amount of forum posts a user has made. (Good for when a free web host requires a user to make a certain amount of forum posts before requesting free hosting)
    * Affiliate program
    * Automatic suspension of accounts which have not paid their monthly fee after 30 days of the invoice email being sent
    * Instant activation of paid hosting accounts
    * Upgrading of free hosting accounts on request or once they've made enough forum posts
    * Customisable header, footer and CSS on the order form
    * Adding hosting packages and changing their prices
    * View your recent transactions and how much each one paid you
    * View the personal details of each one of your customers
    * Change the currency you wish to sell in
    * Cron the renewal emails, to send automatic emails to customers to pay their monthly fee
    * Suspend and unsuspend hosting accounts in the admin panel
    * Responsive design
    * Add a ticket system, so customer support is available within the Hostclaw script
    * Add support for Stripe payment processor
    * Allow users to pay annually instead of monthly, and allow customers (and admins) to change their payment cycle at will
    * Improve the design of the script
    * Try to reduce the amount of pages the admin panel has
    Client area
    * Allow users to get free hosting packages in return for forum posts
    * Check who has done their monthly forum posts if needed

## Forum scripts supported

    * phpBB
    * MyBB

## Payment processors supported

    * PayPal
    * Moneybookers/Skrill
    * Stripe
    * Paymentwall

